package mathhelper.twf.platformdependent

import kotlin.js.Json

fun entriesOf(jsObject: dynamic): List<Pair<String, Any?>> =
        (js("Object.entries") as (dynamic) -> Array<Array<Any?>>)
                .invoke(jsObject)
                .map { entry -> entry[0] as String to entry[1] }

fun mapOf(jsObject: dynamic): Map<String, Any?> =
        entriesOf(jsObject).toMap()


class JsonParser {
    companion object {
        fun parseMap(json: String): Map<String, Any> {
            val jsonObj = JSON.parse<Json>(json)
//            console.log("jsonObj", jsonObj)
            return parseMapRecursive(jsonObj)
        }

        fun parseMapRecursive(jsonObj: Json): Map<String, Any> {
            val res = mutableMapOf<String, Any>()
            val jsonMap = entriesOf(jsonObj).toMap()
//            console.log("jsonMap", jsonMap)
            for ((key, valueObj) in jsonMap) {
//                console.log("key", key)
//                console.log("valueObj", valueObj)
                if (valueObj != null) {
                    val value = when (valueObj) {
                        is String, Int, Long, Double, Float -> {
//                            console.log("String, Int, Long, Double, Float")
                            valueObj
                        }
                        is Array<*> -> {
//                            console.log("is Collection")
                            valueObj.map {
                                when (it) {
                                    is String, Int, Long, Double, Float -> {
                                        it
                                    }
                                    else -> parseMapRecursive(it as Json)
                                }
                            }
                        }
                        else -> {
//                            console.log("else")
                            parseMapRecursive(valueObj as Json)
                        }
                    }
//                    console.log("value", value)
                    res.put(key, value)
                }
            }
            return res
        }
    }
}
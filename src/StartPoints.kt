import mathhelper.twf.api.*
import mathhelper.twf.config.*
import mathhelper.twf.defaultcontent.ExpressionFrontInput
import mathhelper.twf.defaultcontent.toStructureStructureString
import mathhelper.twf.expressiontree.*
import mathhelper.twf.logs.MessageType
import mathhelper.twf.logs.log
import mathhelper.twf.mainpoints.TexVerificationResult
import mathhelper.twf.mainpoints.checkFactsInMathML
import mathhelper.twf.mainpoints.configSeparator
import mathhelper.twf.platformdependent.JsonParser
import mathhelper.twf.platformdependent.mapOf
import kotlin.js.Json


//compiled configuration
@JsName("createCompiledConfigurationFromExpressionSubstitutionsAndParams")
fun createCompiledConfigurationFromExpressionSubstitutionsAndParams_JS(
        expressionSubstitutions: Array<ExpressionSubstitution>,
        additionalParamsMap: Map<String, String> = mapOf()
) = createCompiledConfigurationFromExpressionSubstitutionsAndParams(expressionSubstitutions, additionalParamsMap)

@JsName("createConfigurationFromRulePacksAndParams")
fun createConfigurationFromRulePacksAndParams_JS(
        rulePacks: Array<String> = listOf("Algebra").toTypedArray(),
        additionalParamsMap: Map<String, String> = mapOf()
) = createConfigurationFromRulePacksAndParams(rulePacks, additionalParamsMap)

@JsName("createConfigurationFromRulePacksAndDetailSolutionCheckingParams")
fun createConfigurationFromRulePacksAndDetailSolutionCheckingParams_JS(
        rulePacks: Array<String> = listOf("Algebra").toTypedArray(),
        wellKnownFunctionsString: String = "${configSeparator}0$configSeparator${configSeparator}1$configSeparator+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1$configSeparator^$configSeparator-1",
        expressionTransformationRulesString: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        maxExpressionTransformationWeight: String = "1.0",
        unlimitedWellKnownFunctionsString: String = wellKnownFunctionsString,
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        maxDistBetweenDiffSteps: String = "", //is it allowed to differentiate expression in one step
        scopeFilter: String = "", //subject scopes which user representation sings is used
        wellKnownFunctions: Array<FunctionIdentifier> = arrayOf(),
        unlimitedWellKnownFunctions: Array<FunctionIdentifier> = wellKnownFunctions,
        expressionTransformationRules: Array<ExpressionSubstitution> = arrayOf() //full list of expression transformations rules
) = createConfigurationFromRulePacksAndDetailSolutionCheckingParams(
        rulePacks,
        wellKnownFunctionsString,
        expressionTransformationRulesString,
        maxExpressionTransformationWeight,
        unlimitedWellKnownFunctionsString,
        taskContextExpressionTransformationRules,
        maxDistBetweenDiffSteps,
        scopeFilter,
        wellKnownFunctions.toList(),
        unlimitedWellKnownFunctions.toList(),
        expressionTransformationRules.toList()
)

//expressions
@JsName("stringToExpression")
fun stringToExpression_JS(
        string: String,
        scope: String = "",
        format: String = "", //"MathML", "Tex" ("PlainText" by default)
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = stringToExpression(string, scope, format, functionConfiguration, compiledConfiguration)

@JsName("structureStringToExpression")
fun structureStringToExpression_JS(
        structureString: String,
        scope: String = "",
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        )
) = structureStringToExpression(structureString, scope, functionConfiguration)


@JsName("cloneExpression")
fun cloneExpression_JS(
        expressionNode: ExpressionNode
) = expressionNode.clone()


@JsName("expressionToString")
fun expressionToString_JS(
        expressionNode: ExpressionNode,
        characterEscapingDepth: Int = 1
) = expressionToString(expressionNode, characterEscapingDepth)

@JsName("expressionToUnicodeString")
fun expressionToUnicodeString_JS(
        expressionNode: ExpressionNode,
        characterEscapingDepth: Int = 1
) = expressionToUnicodeString(expressionNode, characterEscapingDepth)

@JsName("expressionToTexString")
fun expressionToTexString_JS(
        expressionNode: ExpressionNode,
        characterEscapingDepth: Int = 1
) = expressionToTexString(expressionNode, characterEscapingDepth)

@JsName("expressionToStructureString")
fun expressionToStructureString_JS(
        expressionNode: ExpressionNode
) = expressionToStructureString(expressionNode)

@JsName("stringToStructureString")
fun stringToStructureString_JS(
        string: String,
        scope: String = "",
        format: String = "", //"MathML", "Tex" ("PlainText" by default)
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = stringToStructureString(string, scope, format, functionConfiguration, compiledConfiguration)

//substitutions
@JsName("expressionSubstitutionFromStrings")
fun expressionSubstitutionFromStrings(
        left: String,
        right: String,
        scope: String = "",
        basedOnTaskContext: Boolean = false,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(
                scopeFilter = scope.split(";").filter { it.isNotEmpty() }.toSet()
        ),
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = mathhelper.twf.api.expressionSubstitutionFromStrings(left, right, scope, basedOnTaskContext, functionConfiguration = functionConfiguration, compiledConfiguration = compiledConfiguration)

@JsName("expressionSubstitutionFromStructureStrings")
fun expressionSubstitutionFromStructureStrings_JS(
        leftStructureString: String = "",
        rightStructureString: String = "",
        basedOnTaskContext: Boolean = false,
        matchJumbledAndNested: Boolean = false,
        simpleAdditional: Boolean = false,
        isExtending: Boolean = false,
        priority: Int = 50,
        code: String = "",
        nameEn: String = "",
        nameRu: String = ""
) = expressionSubstitutionFromStructureStrings(
        leftStructureString,
        rightStructureString,
        basedOnTaskContext,
        matchJumbledAndNested,
        simpleAdditional,
        isExtending,
        priority,
        code,
        nameEn,
        nameRu
)

@JsName("findApplicableSubstitutionsInSelectedPlace")
fun findApplicableSubstitutionsInSelectedPlace_JS(
        expression: ExpressionNode,
        selectedNodeIds: Array<Int>,
        compiledConfiguration: CompiledConfiguration,
        simplifyNotSelectedTopArguments: Boolean = false,
        withReadyApplicationResult: Boolean = true
) = findApplicableSubstitutionsInSelectedPlace(expression, selectedNodeIds, compiledConfiguration, simplifyNotSelectedTopArguments, withReadyApplicationResult)
        .toTypedArray()

@JsName("findSubstitutionPlacesInExpression")
fun findSubstitutionPlacesInExpression(
        expression: ExpressionNode,
        substitution: ExpressionSubstitution
) = mathhelper.twf.api.findSubstitutionPlacesInExpression(expression, substitution).toTypedArray();

@JsName("applySubstitution")
fun applySubstitution(
        expression: ExpressionNode,
        substitution: ExpressionSubstitution,
        substitutionPlaces: Array<SubstitutionPlace> //containsPointersOnExpressionPlaces
) = mathhelper.twf.api.applySubstitution(expression, substitution, substitutionPlaces.toList())

//check solution
@JsName("checkSolutionInTex")
fun checkSolutionInTex_JS(
        originalTexSolution: String, //string with learner solution in Tex format

        //// individual task parameters:
        startExpressionIdentifier: String = "", //Expression, from which learner need to start the transformations
        targetFactPattern: String = "", //Pattern that specify criteria that learner's answer must meet
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here, that can be used as rules only for this task

        endExpressionIdentifier: String = "", //Expression, which learner need to deduce
        targetFactIdentifier: String = "", //Fact that learner need to deduce. It is more flexible than startExpressionIdentifier and endExpressionIdentifier, allow to specify inequality like '''EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{<=}{(/(2;sin(x)))}'''
        comparisonSign: String = "", //Comparison sign

        //// general configuration parameters
        //functions, which null-weight transformations allowed (if no other transformations), split by configSeparator
        //choose one of 2 api forms:
        wellKnownFunctions: Array<FunctionIdentifier> = arrayOf(),
        wellKnownFunctionsString: String = "${configSeparator}0$configSeparator${configSeparator}1$configSeparator+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1$configSeparator^$configSeparator-1",

        //functions, which null-weight transformations allowed with any other transformations, split by configSeparator
        //choose one of 2 api forms:
        unlimitedWellKnownFunctions: Array<FunctionIdentifier> = wellKnownFunctions,
        unlimitedWellKnownFunctionsString: String = wellKnownFunctionsString,

        //expression transformation rules
        //choose one of api forms:
        expressionTransformationRules: Array<ExpressionSubstitution> = arrayOf(), //full list of expression transformations rules
        expressionTransformationRulesString: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        rulePacks: Array<String> = listOf<String>().toTypedArray(),

        maxExpressionTransformationWeight: String = "1.0",
        maxDistBetweenDiffSteps: String = "", //is it allowed to differentiate expression in one step
        scopeFilter: String = "", //subject scopes which user representation sings is used

        shortErrorDescription: String = "0" //make error message shorter and easier to understand: crop parsed steps from error description
) = checkSolutionInTex(
        originalTexSolution,

        startExpressionIdentifier,
        targetFactPattern,
        additionalFactsIdentifiers,

        endExpressionIdentifier,
        targetFactIdentifier,
        comparisonSign,

        wellKnownFunctions.toList(),
        wellKnownFunctionsString,

        unlimitedWellKnownFunctions.toList(),
        unlimitedWellKnownFunctionsString,

        expressionTransformationRules.toList(),
        expressionTransformationRulesString,
        taskContextExpressionTransformationRules,
        rulePacks,

        maxExpressionTransformationWeight,
        maxDistBetweenDiffSteps,
        scopeFilter,

        shortErrorDescription
)

@JsName("checkSolutionInTexWithCompiledConfiguration")
fun checkSolutionInTexWithCompiledConfiguration_JS(
        originalTexSolution: String, //string with learner solution in Tex format
        compiledConfiguration: CompiledConfiguration,

        //// individual task parameters:
        startExpressionIdentifier: String = "", //Expression, from which learner need to start the transformations
        targetFactPattern: String = "", //Pattern that specify criteria that learner's answer must meet
        comparisonSign: String,
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here, that can be used as rules only for this task

        endExpressionIdentifier: String = "", //Expression, which learner need to deduce
        targetFactIdentifier: String = "", //Fact that learner need to deduce. It is more flexible than startExpressionIdentifier and endExpressionIdentifier, allow to specify inequality like '''EXPRESSION_COMPARISON{(+(/(sin(x);+(1;cos(x)));/(+(1;cos(x));sin(x))))}{<=}{(/(2;sin(x)))}'''

        shortErrorDescription: String = "0" //make error message shorter and easier to understand: crop parsed steps from error description
): TexVerificationResult {
    return checkSolutionInTexWithCompiledConfiguration(
            originalTexSolution,
            compiledConfiguration,
            startExpressionIdentifier,
            targetFactPattern,
            comparisonSign,
            additionalFactsIdentifiers,
            endExpressionIdentifier,
            targetFactIdentifier,
            shortErrorDescription)
}

@JsName("createCompiledConfigurationFromITR")
fun createCompiledConfigurationFromITR_JS(
        taskITR: TaskITR, //solving task
        rulePacksITR: Array<RulePackITR>, //corresponding rule packs
        comparisonSettings: ComparisonSettings = ComparisonSettings()
) = createCompiledConfigurationFromITR(taskITR, rulePacksITR, comparisonSettings)


@JsName("checkSolutionInTexITR")
fun checkSolutionInTexITR_JS(
        originalTexSolution: String, //string with learner solution in Tex format
        taskITR: TaskITR, //solving task
        rulePacksITR: Array<RulePackITR>, //corresponding rule packs
        shortErrorDescription: String = "0", //make error message shorter and easier to understand: crop parsed steps from error description
        skipTrivialCheck: Boolean = false, //do not check completeness of transformations, only correctness,
        comparisonSettings: ComparisonSettings = ComparisonSettings(),
        inputCompiledConfiguration: CompiledConfiguration? = null // can be specified to make verification process faster
) = checkSolutionInTexITR(originalTexSolution, taskITR, rulePacksITR, shortErrorDescription, skipTrivialCheck,
comparisonSettings, inputCompiledConfiguration)

@JsName("getParsedExpressionByMathML")
fun getParsedExpressionByMathML(mathML: String): String {
    val expressionTreeParser = ExpressionTreeParser(mathML)
    expressionTreeParser.parse()
    val root = expressionTreeParser.root
    return root.toString()
}

@JsName("checkFactsInMathML")
fun checkFactsInMathML(
        brushedMathML: String,
        wellKnownFunctions: String = "+$configSeparator-1$configSeparator-$configSeparator-1$configSeparator*$configSeparator-1$configSeparator/$configSeparator-1", //functions, which null-weight transformations allowed (if no other transformations), split by configSeparator
        expressionTransformationRules: String = "S(i, a, a, f(i))${configSeparator}f(a)${configSeparator}S(i, a, b, f(i))${configSeparator}S(i, a, b-1, f(i)) + f(b)", //function transformation rules, parts split by configSeparator; if it equals " " then expressions will be checked only by testing
        targetFactIdentifier: String = "", //Fact that learner need to prove should be here
        targetVariablesNames: String = "", //Variables expressions for which learner need to deduce, split by configSeparator
        minNumberOfMultipliersInAnswer: String = "", //For factorization tasks
        maxNumberOfDivisionsInAnswer: String = "", //For fraction reducing tasks
        additionalFactsIdentifiers: String = "", ///Identifiers split by configSeparator - task condition facts should be here
        maxExpressionTransformationWeight: String = "1.0",
        unlimitedWellKnownFunctions: String = wellKnownFunctions, //functions, which null-weight transformations allowed with any other transformations, split by configSeparator
        shortErrorDescription: String = "0", //crop parsed steps from error description
        taskContextExpressionTransformationRules: String = "", //for expression transformation rules based on variables
        allowedVariablesNames: String = "", //Variables expressions for which learner need to deduce, split by configSeparator
        maxDistBetweenDiffSteps: String = "", //is it allowed to differentiate expression in one step
        forbiddenFunctions: String = "", //functions cannot been used in answer
        scopeFilter: String = "", //subject scopes which user representation sings is used
        makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts: String = "0" // if true, it is possible to define custom condition to enrich known facts for transformations
) = mathhelper.twf.mainpoints.checkFactsInMathML(
        brushedMathML,
        wellKnownFunctions,
        expressionTransformationRules,
        targetFactIdentifier,
        targetVariablesNames,
        minNumberOfMultipliersInAnswer,
        maxNumberOfDivisionsInAnswer,
        additionalFactsIdentifiers,
        maxExpressionTransformationWeight,
        unlimitedWellKnownFunctions,
        shortErrorDescription,
        taskContextExpressionTransformationRules,
        allowedVariablesNames,
        maxDistBetweenDiffSteps,
        forbiddenFunctions,
        scopeFilter,
        makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts
)

@JsName("getUserLogInPlainText")
fun getUserLogInPlainText() = log.getLogInPlainText(MessageType.USER)

@JsName("getUserLogInJson")
fun getUserLogInJson() = log.getLogInJson(MessageType.USER)

@JsName("getAllLogInPlainText")
fun getAllLogInPlainText() = log.getLogInPlainText()

@JsName("getAllLogInJson")
fun getAllLogInJson() = log.getLogInJson()

@JsName("encryptSolution")
fun encryptSolution(solution: String) = "${solution.hashCode()}_${solution.length}"

@JsName("encryptResult")
fun encryptResult(result: String, solution: String) = "$result ${solution.hashCode()}_${solution.length}"


@JsName("decodeUrlSymbols")
fun decodeUrlSymbols_JS(string: String) = decodeUrlSymbols(string)


//compare expressions without substitutions
@JsName("compareWithoutSubstitutions")
fun compareWithoutSubstitutions(
        left: ExpressionNode,
        right: ExpressionNode,
        scope: Set<String> = setOf(""),
        notChangesOnVariablesFunction: Set<String> = setOf("+", "-", "*", "/", "^"),
        maxExpressionBustCount: Int = 4096,
        functionConfiguration: FunctionConfiguration = FunctionConfiguration(scope, notChangesOnVariablesFunction),
        comparisonSettings: ComparisonSettings = ComparisonSettings().apply { this.maxExpressionBustCount = maxExpressionBustCount},
        compiledConfiguration: CompiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration)
) = mathhelper.twf.api.compareWithoutSubstitutions(left, right, scope, notChangesOnVariablesFunction, maxExpressionBustCount, functionConfiguration, comparisonSettings, DebugOutputMessages(), compiledConfiguration)

@JsName("createExpressionFrontInput")
fun createExpressionFrontInput_JS(expression: String, format: String) = createExpressionFrontInput(expression, format)

@JsName("createRuleITR")
fun createRuleITR_JS(
        code: String? = null,
        nameEn: String? = null,
        nameRu: String? = null,
        descriptionShortEn: String? = null,
        descriptionShortRu: String? = null,
        descriptionEn: String? = null,
        descriptionRu: String? = null,

        left: ExpressionFrontInput? = null,
        right: ExpressionFrontInput? = null,
        priority: Int? = null,
        isExtending: Boolean? = null,
        matchJumbledAndNested: Boolean? = null,
        simpleAdditional: Boolean? = null,
        basedOnTaskContext: Boolean? = null,
        normalizationType: String? = null,
        weight: Double? = null,
        subjectType: String? = null
) = createRuleITR(
        code, nameEn, nameRu, descriptionShortEn, descriptionShortRu, descriptionEn, descriptionRu,
        left, right, priority, isExtending, matchJumbledAndNested, simpleAdditional, basedOnTaskContext, normalizationType, weight,
        subjectType
)

@JsName("createRulePackLinkITR")
fun createRulePackLinkITR_JS(
        namespaceCode: String? = null,
        rulePackCode: String? = null
) = createRulePackLinkITR(
        namespaceCode, rulePackCode
)

@JsName("createRulePackITR")
fun createRulePackITR_JS(
        code: String? = null,
        version: Int = 0,
        namespaceCode: String? = null,
        nameEn: String? = null,
        nameRu: String? = null,
        descriptionShortEn: String? = null,
        descriptionShortRu: String? = null,
        descriptionEn: String? = null,
        descriptionRu: String? = null,

        subjectType: String = "standard_math",
        rulePacks: Array<RulePackLinkITR>? = null,
        rules: Array<RuleITR>? = null,

        otherCheckSolutionData: String? = null, //parameters in task redefine parameters in this map
        otherAutoGenerationData: String? = null,
        otherData: String? = null
) = createRulePackITR(
        code, version, namespaceCode, nameEn, nameRu, descriptionShortEn, descriptionShortRu, descriptionEn, descriptionRu,
        subjectType, rulePacks?.toList(), rules?.toList(),
        otherCheckSolutionData, otherAutoGenerationData, otherData
)

@JsName("createTaskITR")
fun createTaskITR_JS (
        taskCreationType: String = "manual",

        code: String? = null,
        namespaceCode: String? = null,
        nameEn: String? = null,
        nameRu: String? = null,
        descriptionShortEn: String? = null,
        descriptionShortRu: String? = null,
        descriptionEn: String? = null,
        descriptionRu: String? = null,

        subjectType: String? = null,
        tags: Array<String>? = null,

        originalExpression: ExpressionFrontInput? = null,

        goalType: String? = null,
        goalExpression: ExpressionFrontInput? = null,
        goalPattern: String? = null,
        goalNumberProperty: Int? = null,
        otherGoalData: String? = null,

        rulePacks: Array<RulePackLinkITR>? = null,
        rules: Array<RuleITR>? = null,

        stepsNumber: Int? = null,
        time: Int? = null,
        difficulty: Double,

        solution: ExpressionFrontInput? = null,
        solutionsStepsTree: String? = null,

        interestingFacts: String? = null,
        nextRecommendedTasks: String? = null,

        hints: String? = null,
        otherCheckSolutionData: String? = null,

        countOfAutoGeneratedTasks: Int? = 0,
        otherAutoGenerationData: String? = null,

        otherAwardData: String? = null,
        otherData: String? = null
) = createTaskITR(
        taskCreationType, code, namespaceCode, nameEn, nameRu, descriptionShortEn, descriptionShortRu, descriptionEn, descriptionRu,
        subjectType, tags, originalExpression, goalType, goalExpression, goalPattern, goalNumberProperty, otherGoalData,
        rulePacks, rules,
        stepsNumber, time, difficulty, solution,
        solutionsStepsTree, interestingFacts, nextRecommendedTasks, hints, otherCheckSolutionData,
        countOfAutoGeneratedTasks, otherAutoGenerationData, otherAwardData, otherData
)

@JsName("arrayToList")
fun arrayToList_JS(data: Array<*>) = data.toList()

@JsName("listToArray")
fun listToArray_JS(data: List<*>) = data.toTypedArray()

@JsName("arrayToSet")
fun arrayToSet_JS(data: Array<*>) = data.toSet()

@JsName("dinamicToMap")
fun dinamicToMap_JS(data: dynamic) = mapOf(data)

@JsName("jsonStringToMap")
fun jsonStringToMap_JS(json: String) = JsonParser.parseMap(json)

@JsName("jsonToMap")
fun jsonToMap_JS(json: Json) = JsonParser.parseMapRecursive(json)

@JsName("generateTasks")
fun generateTasks_JS(
        area: String,
        startExpression: String,
        rulepacks: Array<RulePackITR> = arrayOf(),
        additionalParamsJsonString: String = "{}"
) = generateTasks(area, startExpression, rulepacks, additionalParamsJsonString)

@JsName("getAllTagsForGeneration")
fun getAllTagsForGeneration_JS(area: String) = getAllTagsForGeneration(area)

@JsName("getAllSortTypesForGeneration")
fun getAllSortTypesForGeneration_JS() = getAllSortTypesForGeneration()

@JsName("getAllSortOrdersForGeneration")
fun getAllSortOrdersForGeneration_JS() = getAllSortOrdersForGeneration()

@JsName("getLogOfGeneration")
fun getLogOfGeneration_JS() = getLogOfGeneration()

@JsName("getReportOfGeneration")
fun getReportOfGeneration_JS() = getReportOfGeneration()
